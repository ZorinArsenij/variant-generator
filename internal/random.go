package internal

import (
	"crypto/sha256"
	"encoding/binary"

	"github.com/spaolacci/murmur3"
)

func GetRandomVariant(value []byte, num, seed uint64) uint64 {
	hash := sha256.New()
	hash.Write(value)

	s := make([]byte, 8)
	binary.LittleEndian.PutUint64(s, seed)

	m := murmur3.New64WithSeed(uint32(seed))
	m.Write(hash.Sum(s))

	return (m.Sum64() % num) + 1
}
