package main

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/ZorinArsenij/variant-generator/internal"
)

func main() {
	app := cli.NewApp()

	app.Name = "VariantGenerator"
	app.Usage = "CLI application for generation random variant for every student in received list"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:     "file",
			Aliases:  []string{"f"},
			Usage:    "Path to file with student list",
			Required: true,
		},
		&cli.Uint64Flag{
			Name:     "numbilets",
			Aliases:  []string{"n"},
			Usage:    "Number of billets",
			Required: true,
		},
		&cli.Uint64Flag{
			Name:    "parameter",
			Aliases: []string{"p"},
			Usage:   "Parameter for randomizing",
			Value:   42,
		},
	}

	app.Action = func(c *cli.Context) error {
		filepath := c.String("file")
		num := c.Uint64("numbilets")
		seed := c.Uint64("parameter")

		file, err := os.Open(filepath)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			bytes := scanner.Bytes()
			fmt.Printf("%s: %d\n", string(bytes), internal.GetRandomVariant(bytes, num, seed))
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}

		return nil
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
