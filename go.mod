module gitlab.com/ZorinArsenij/variant-generator

go 1.13

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/spaolacci/murmur3 v1.1.0
	github.com/urfave/cli/v2 v2.2.0
)
