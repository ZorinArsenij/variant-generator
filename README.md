# Variant generator

Command line program for generation pseudorandom variants for received student list

# Build
Install Golang:
```bash
curl -O https://storage.googleapis.com/golang/go1.13.5.linux-amd64.tar.gz
tar -xvf go1.13.5.linux-amd64.tar.gz
sudo mv go /usr/local
```
Build code:
```bash
go build -o program cmd/main.go
```

# Run
Example of command for running program:
```bash
./program --file example.txt --numbilets 20 --parameter 44
```

Options:
```bash
   --file value, -f value       Path to file with student list
   --numbilets value, -n value  Number of billets (default: 0)
   --parameter value, -p value  Parameter for randomizing (default: 42)
   --help, -h                   show help (default: false)
```

Additional CLI usage options are available and may be shown by running `./program --help`.